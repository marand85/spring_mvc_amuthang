package com.packt.webstore.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.repository.CustomerRepository;

@Repository
public class InMemoryCustomerRepository implements CustomerRepository {

	private List<Customer> listOfCustomers = new ArrayList<Customer>();
	
	public InMemoryCustomerRepository() {
		
		Customer customer1 = new Customer("K0001","Patrycja Woźniak");//, "51-170 Wrocław", "ul. Gimnazjalna 84", 3);
		
		Customer customer2 = new Customer("K0002","Wiktoria Chmielewska");//, "35-002 Rzeszów", "Pl. Ofiar Getta 106", 5);
		
		Customer customer3 = new Customer("K0003","Jaropełk Borkowski");//, "85-900 Bydgoszcz", "ul. Zygmunta Augusta 61", 7);
		
		Customer customer4 = new Customer("K0004","Ksenia Zając");//, "01-126 Warszawa", "ul. Wolska 136", 2);
		
		Customer customer5 = new Customer("K0005","Kuba Wojciechowski");//, "06-516 Szydłowo", "ul. Szydłowo koło Mławy 126", 4);
		
		Customer customer6 = new Customer("K0006","Sergiusz Kozłowski");//, "31-988 Kraków", "ul. Łozińskiego Władysława 121", 8);
		
		Customer customer7 = new Customer("K0007","Zbigniew Nowicki");//, "70-832 Szczecin", "ul. Pokładowa 122", 5);
			
		listOfCustomers.add(customer1);
		listOfCustomers.add(customer2);
		listOfCustomers.add(customer3);
		listOfCustomers.add(customer4);
		listOfCustomers.add(customer5);
		listOfCustomers.add(customer6);
		listOfCustomers.add(customer7);
	}
	
	public List<Customer> getAllCustomers() {
		return listOfCustomers;
	}
	
	public Customer getCustomerById(String customerId) {
		Customer customerById = null;
		for(Customer customer : listOfCustomers) {
			if(customer!=null && customer.getCustomerId()!=null && customer.getCustomerId().equals(customerId)){
				customerById = customer;
				break;
			}
		}
		if(customerById == null) {
			throw new IllegalArgumentException("Brak klienta o wskazanym id: "+ customerId);
		}
		return customerById;
	}
}