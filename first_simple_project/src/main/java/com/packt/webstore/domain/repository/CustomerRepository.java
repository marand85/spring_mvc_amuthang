package com.packt.webstore.domain.repository;

import java.util.List;
import com.packt.webstore.domain.*; 

public interface CustomerRepository {
	public List<Customer> getAllCustomers();
	
	public Customer getCustomerById(String customerId);
}
