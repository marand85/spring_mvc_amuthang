package com.packt.webstore.domain.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.packt.webstore.domain.*; 

public interface ProductRepository {
	public List<Product> getAllProducts();
	
	public Product getProductById(String productId);
	
	public List<Product> getProductsByCategory(String category);
	
	public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams);
	
	public List<Product> getProductsByManufacturer(String manufacturer);
	
	public Set<Product> getProductsByPriceFilter(Map<String, List<String>> filterParams);
	
	public void addProduct(Product product);
}