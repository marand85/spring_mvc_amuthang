package com.packt.webstore.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.packt.webstore.domain.*; 

public interface ProductService {
	public List<Product> getAllProducts();
	public Product getProductById(String productID);
	public List<Product> getProductsByCategory(String category); 
	public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams);
	public List<Product> getProductsByManufacturer(String manufacturer);
	public Set<Product> getProductsByPriceFilter(Map<String, List<String>> filterParams);
	public void addProduct(Product product);
}
