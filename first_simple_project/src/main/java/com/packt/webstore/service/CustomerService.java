package com.packt.webstore.service;

import java.util.List;
import com.packt.webstore.domain.*; 

public interface CustomerService {
	public List<Customer> getAllCustomers();
}
